from pylab import *
from collections import OrderedDict

file = open("dataCovid.csv", "r")
valuesHospi = OrderedDict()
valuesRea = OrderedDict()
file.readline()

for line in file:
  parsedLine = line.split(";")
  try:
    valuesHospi[parsedLine[1]] += int(parsedLine[2])
    valuesRea[parsedLine[1]] += int(parsedLine[3])
  except:
    valuesHospi[parsedLine[1]] = int(parsedLine[2])
    valuesRea[parsedLine[1]] = int(parsedLine[3])

def plotHospi():
    figure("Nouvelles admissions journalieres a l'hopital")
    x, y = zip(*valuesHospi.items())
    bars = matplotlib.pyplot.bar(x,y)

    for bar in bars:
      height = bar.get_height()
      annotate(str(height),xy=(bar.get_x()+bar.get_width() / 2, height))
    xticks(rotation=75)

def plotRea():
    figure("Nouvelles admissions journalieres en reanimation")
    x, y = zip(*valuesRea.items())
    bars2 = matplotlib.pyplot.bar(x,y)

    for bar in bars2:
      height = bar.get_height()
      annotate(str(height),xy=(bar.get_x()+bar.get_width() / 2, height))
    xticks(rotation=75)

plotHospi()
plotRea()
show()
